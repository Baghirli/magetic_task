from setuptools import setup


setup(
    name="magetic_test",
    description='parser for play.google',
    install_requires=["requests", "beautifulsoup4"],
    zip_safe=False,
    enrty_points={
        "console_scripts": ["play_google_parser=scripts.parser:entrypoint"],
    },
)
