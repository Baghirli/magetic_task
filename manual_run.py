import sys
import scripts.parser as parser

def parse_line(line):
    pair = line.strip().split("=")
    pair[0] = f"--{pair[0]}"
    return pair

def get_config(filename):
    config_file = open(filename, "r")
    options = config_file.readlines()
    arglist = []
    [arglist.extend(parse_line(o)) for o in options]
    parser.entrypoint(arglist)

if len(sys.argv) == 1:
    get_config("config.txt")
elif sys.argv[1] == '--c':
    get_config(sys.argv[2])
else:
    parser.entrypoint()

