from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
from argparse import ArgumentParser
import logging, sys

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


class ParseHTML():

    __instance = None

    def __init__(self, options, gethtml=True):
        if ParseHTML.__instance:
            self = ParseHTML.__instance
        else:
            ParseHTML.__instance = self
            self.parse_address = options.parse_address
            if gethtml:
                self.set_bs_tree()

    def set_bs_tree(self):
        self.bs_tree = BeautifulSoup(self._get(), "html.parser")

    def set_address(self, address):
        self.parse_address = address

    def _get(self, url=None):
        """
            Attempts to get the content at `url` by making an HTTP GET request.
            If the content-type of response is some kind of HTML/XML, return the
            text content, otherwise return None.
            """
        if not url:
            url = self.parse_address
        try:
            with closing(get(url, stream=True)) as resp:
                if is_good_response(resp):
                    return resp.content
                else:
                    return None

        except RequestException as e:
            logger.info('Error during requests to {0} : {1}'.format(url, str(e)))
            return None

    def get_tag_element_list(self, tag):
        """search by tag"""
        return self.bs_tree.findAll(tag)

    def get_class_element_list(self, tag_class):
        """search by class"""
        return self.bs_tree.findAll(class_=tag_class)

    def get_custom_selection(self, method, tag, kwargs=None):
        """selection dict structure:
            method : Beautifulsoup method
            tag: positional argument (tag)
            kwargs : dict with keyword arguments names as keys
            example: get_custom_selection('findAll',
                                          ['a'],
                                          {'class_': 'leaf-submenu-link'})
            if no kwargs - pass empty dict
        """
        bs_method = getattr(self.bs_tree, method)

        return bs_method(tag, **kwargs or {})


class GetGamesAppCategory():
    def __init__(self, html_parser, options):
        self.html_parser = html_parser
        self._set_categories(options.category_tag, options.category_class)

    def _set_categories(self, category_tag, category_class):
        self.categories_tags = self.html_parser.get_custom_selection(
            'findAll',
            category_tag,
            {'class_': category_class})
        self.categories_list = [cat.attrs['href'].split('/')[-1] for cat in self.categories_tags]



class GetGamesAppNames():
    def __init__(self, html_parser, options):
        self.html_parser = html_parser
        self._set_games(options.game_category_tag, options.game_category_class)

    def _set_games(self, game_tag, game_class):
        self.games_tags = self.html_parser.get_custom_selection(
            'findAll',
            [game_tag],
            {'class_': game_class})
        self.games_list = ['/'.join([tag.findAll('a', class_="title")[0].attrs['title'], tag.attrs["data-docid"]]) for tag in self.games_tags]


def entrypoint(args=None):
    parser = ArgumentParser()
    parser.add_argument("--parse_address", type=str)
    parser.add_argument("--category_tag", type=str)
    parser.add_argument("--category_class", type=str)
    parser.add_argument("--game_category_tag", type=str)
    parser.add_argument("--game_category_class", type=str)
    options = parser.parse_args(args if args else sys.argv[1:])
    myhtml = ParseHTML(options)
    GameCategory = GetGamesAppCategory(myhtml, options)
    GameClass = GetGamesAppNames(myhtml, options)
    cust1 = myhtml.get_custom_selection('findAll', ['a'], {'class_': 'leaf-submenu-link'})
    a=1   # for breakpoint