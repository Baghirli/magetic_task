# Magetic test task

> Magetic test task

installation:
```sh
pip install -r requirements.txt

pip install -e .
```

usage:

if not installed use manual.py:

With config file. Will read arguments from specified configuration file. Absolute and relative paths supported.
```sh
python manual_run.py --c config.txt

```

With default configuration. Will use config.txt file in same directory.
```sh
Python manual_run.py

```

if installed use console command play_google_parser with arguments listed in config.txt

